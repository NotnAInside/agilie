import mongoose from 'mongoose';
import * as dotenv from 'dotenv';
dotenv.config();

export const connectDb = async function () {
    await mongoose.connect(process.env.MONGO_URI);
}
