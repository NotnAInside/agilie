export interface ICreateAppointment {
    patientId: string;
    doctorId: string;
    startDate: Date;
    endDate: Date;
}