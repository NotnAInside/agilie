import { NextFunction, Request, Response } from "express";
import { AppointmentService } from "../services/appointment.service";
import { ICreateAppointment } from "../interfaces/interface";
import { NotificationService } from "../services/notification.service";

export const route = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
        const params = { ...req.params, ...req.query, ...req.body } as ICreateAppointment;
        const appointmentService = new AppointmentService();
        const appointmentList = await appointmentService.findByDoctorOrPatientIds(params);
        if (appointmentList.length > 0) {
            throw new Error('You cant bind appointment at this time');
        }
        const appointment = await appointmentService.create(params);
        const notificationService = new NotificationService();
        const motificationDates = await notificationService.getDates(params.startDate);
        // const notifiacations = [];
        for (const motificationDate of motificationDates) {
            await notificationService.create({ notificationTime: motificationDate.notificationTime, appointmentId: appointment.id });
        }
      res.status(200).json(appointment);
    } catch (e) {
      next(e);
    }
  };
  
  export default route;