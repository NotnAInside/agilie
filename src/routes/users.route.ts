import { NextFunction, Request, Response } from "express";
import { UserService } from '../services/users.service';

export const route = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
        const params = { ...req.params, ...req.query, ...req.body };
        const userService = new UserService();
        const user = await userService.create(params);
      res.status(200).json(user);
    } catch (e) {
      next(e);
    }
  };
  
  export default route;