import { model, Schema } from "mongoose";
import { UserRoles } from "../common/enum";

export const UserSchema: Schema = new Schema({
    firstName: String,
    phone: String,
    role: { type: String, enum: Object.values(UserRoles), default: UserRoles.Patient },
  }, { timestamps: true });


export const UserModel = model("User", UserSchema, "users");