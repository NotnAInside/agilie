import { model, Schema } from "mongoose";

export const NotificationSchema: Schema = new Schema({
    appointmentId: String,
    notificationTime: Date,
  }, { timestamps: true });


export const NotificationModel = model("Notifications", NotificationSchema, "notification");