import { model, Schema } from "mongoose";
import { UserRoles } from "../common/enum";

export const AppointmentSchema: Schema = new Schema({
    startDate: Date,
    endDate: Date,
    doctorId: String,
    patientId: String,
    isActive: Boolean
  }, { timestamps: true });


export const AppointmentModel = model("Appointment", AppointmentSchema, "appointment");