import { NotificationModel } from "../models/notification.model";
import * as moment from 'moment';

export class NotificationService {
    async create(params: { appointmentId: string, notificationTime: Date }): Promise<any> {
        return await NotificationModel.create(params);
    }

    async findGreaterOrEqualNotifications(date) {
        return await NotificationModel.find({ notificationTime: { $lte: date }, isActive: true });
    }

    async switch(id: string, isActive: boolean) {
        return await NotificationModel.findByIdAndUpdate(id, { $set: { isActive }}).lean().exec();
    }

    async getDates(date) {
        return [
            { notificationTime: moment(new Date(parseInt(date))).subtract('2', 'hour').toDate() },
            { notificationTime: moment(new Date(parseInt(date))).subtract('1', 'days').toDate()}
        ]
    }
}