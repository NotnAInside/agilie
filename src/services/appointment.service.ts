import { ICreateAppointment } from "../interfaces/interface";
import { AppointmentModel } from "../models/appointment.model";

export class AppointmentService {
    async create(params): Promise<any> {
        return await AppointmentModel.create(params);
    }

    async list(): Promise<any> {
        return await AppointmentModel.find().lean().exec();
    }

    async findById(id: string) {
        return await AppointmentModel.findOne({ _id: id }).lean().exec()
    }

    async findByDoctorOrPatientIds(params: ICreateAppointment): Promise<any> {
        const { doctorId, patientId, startDate, endDate } = params;
        return await AppointmentModel.find({ $or: [{ doctorId }, { patientId }], startDate: { $lte: startDate }, endDate: { $gte: endDate }});
    }
}