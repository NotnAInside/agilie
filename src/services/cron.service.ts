import { CronJob } from 'cron';
import { AppointmentService } from './appointment.service';
import { NotificationService } from './notification.service';
import { UserService } from './users.service';

export class CronService {
    async run(): Promise<void> {
        const notificationService = new NotificationService();
        const appointmentService = new AppointmentService();
        const userService = new UserService();
        const job = new CronJob('* * * * * *', async function() {
            const notifications = await notificationService.findGreaterOrEqualNotifications(new Date());
                for (const notification of notifications) {
                    const appointment = await appointmentService.findById(notification.appointmentId);
                    const user = await userService.findById(appointment?.patientId);
                    console.log(`Hello ${user?.firstName}! Remember that you have an appointment tomorrow!`)

                    await notificationService.switch(notification.id, false)
                }
            },

            null,
            true,
            'America/Los_Angeles'
        );
    }
}