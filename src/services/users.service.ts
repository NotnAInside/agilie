import { UserModel } from "../models/user.model";

export class UserService {
    async create(params): Promise<any> {
        const { firstName, phone, role } = params;
        console.log(params)
       return await UserModel.create(params);
    }

    async findById(id: string) {
        return await UserModel.findById(id);
    }
}