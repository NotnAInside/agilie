import * as express from 'express';
import * as dotenv from 'dotenv';
import * as bodyParser from 'body-parser';
import appoitmentRoute from './routes/appointment.route';
import createUserRoute from './routes/users.route';
import AppointmentRoute from './routes/appointment.route';
import { connectDb } from './db/connection';
import { CronService } from './services/cron.service';
const app = express();
dotenv.config();
connectDb();
const cronService = new CronService();
cronService.run();

app.use(bodyParser.json());
app.get('/', appoitmentRoute);
app.post('/users', createUserRoute);
app.post('/appointment', AppointmentRoute);

app.listen(process.env.PORT);